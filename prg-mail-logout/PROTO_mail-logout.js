(function() {

    var doc = document;

    var prgBranding = doc.createElement('a');
    prgBranding.style.cssText = "z-index: 0; top: 0; right: 0; width: 100%; height: 100%; position: fixed";

    var body = doc.body || doc.getElementsByTagName('body')[0];
    body.style.cssText = "background: url(foo) no-repeat top center";
    body.insertBefore(prgBranding, body.childNodes[0]);

    var pageWrap = doc.querySelector("#pageWrap");
    pageWrap.style.cssText = "position: relative; background: white; top: 100px";

})();