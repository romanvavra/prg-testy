(function() {

    /*
    Vychází ze šablony nakódováné pro přímý obchod ECO_X_mobile_premium_square

    issues: 

     - na ženě ve článcích se může volat pouze na první pozici
     - na ženě v codagraphics je přizpůsobenou pouze na první pozici
     - na vaření > area recepty se může volat pouze na první pozici

     Díky těmto issues se vytvořil pro ženu zařezávací target, kdy na na celé ženě a na celém vaření se premium square volá na pos1
     Target: mobile premium square target - all
    */

    var doc = document;

    var site = "%%SITE%%",
        area = "%%AREA%%",
        template = "%%TEMPLATE%%",
        image = doc.querySelector("#sasia-fcid-%%FCID%%"),
        squareWrapper = doc.querySelector("#proto_prg-premium-square-%%FCID%%"),
        rodic = squareWrapper.parentElement;

    /* CUSTOM CSS PER SITE */

    if (site == "aktualne" && template == "gallery") {
        image.classList.add("mB__20");
    }

    if (site == "hp") {
        squareWrapper.classList.add("mLR__8");
    }

    if (site == "zena") {

        if (template == "article") {

            var cssZena = '#resource-content > p, #resource-content > h3, #resource-content > div, .left-side > div:first-child {margin: 0 20px;} .article.clearfix > .left-side, #reklama-injected-1 {margin: 0 0px;}',
                zenaStyle = document.createElement('style');

            squareWrapper.appendChild(zenaStyle);

            /* Workaround pro mobilní IEčka */
            if (zenaStyle.styleSheet) {
                zenaStyle.styleSheet.cssText = cssZena;
            } else {
                zenaStyle.appendChild(document.createTextNode(cssZena));
            }

        }

        if (template != "article") {
            rodic.parentElement.classList.add("base__width");

            if (template == "graphicscoda") {
                doc.querySelector("#reklama-flow-1").classList.add("base__width");
            }

        }

    }

    if ((site == "ihned" && template == "article") || (site == "ecomag" && area == "ekonom" && template == "article")) {
        rodic.parentElement.classList.add("base__width", "mLR__0");
        image.classList.add("calc__20", "mL__10")
    }

    if (site == "ecomag" && area == "respekt") {
        squareWrapper.classList.add("base__width");
        rodic.classList.add("base__width");
        rodic.parentElement.classList.add("mLR__15");
    }

    if (site == "ecomag" && area == "procne") {

        if (template == "article") {
            image.classList.add("calc__40", "mL__20");
        } else {
            image.classList.add("calc__90", "mL__45");
        }
    }

    if (site == "vybermiauto") {

        if (area == "porovnavac" || area == "clanky-a-recenze") {
            image.classList.add("calc__30", "mL__15");
        } else {
            image.classList.add("calc__0", "mL__0");
        }
    }

    if (site == "vareni") {

        if (area == "ostatni") {
            squareWrapper.classList.add("mB__10");
            image.classlist.add("calc__24", "mL__12");
        }

        if (area == "magazin") {
            image.classList.add("calc__40", "mL__20");
        }

        if (area == "recept") {

            var cssVareni = '.page {padding: 0 0 !important;} .page > header, .page > footer, #recipe-section > .page-detail, #recipe-section > article, .recipe-detail-ingredients, .recipe-detail-desc, recipe-detail-aside {padding: 0 20px !important;}',
                vareniStyle = document.createElement('style');

            squareWrapper.appendChild(vareniStyle);

            /* Workaround pro mobilní IEčka */
            if (vareniStyle.styleSheet) {
                vareniStyle.styleSheet.cssText = cssVareni;
            } else {
                vareniStyle.appendChild(document.createTextNode(cssVareni));
            }
        }

    }

})();