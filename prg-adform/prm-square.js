var prm = document.querySelector("adfm-ad"),
    wrapper = prm.parentNode,
    sDisplay = screen.width,
    site = "%%SITE%%",
    area = "%%AREA%%",
    template = "%%TEMPLATE%%";


// pojistka pro zarovnání u všech služeb + řešení zarování u ženy, respektu, vybermiauto,...    
if (window.matchMedia("(orientation: portrait) and (max-width: 414px)").matches) {
    prm.style.cssText = "left: 50%; margin-left: -" + (sDisplay / 2) + "px";
}

if (window.matchMedia("(orientation: portrait) and (max-width: 1024px)").matches) {
    prm.style.cssText = "max-width: 480px; max-height: 480px; margin-bottom: 20px";
}

if (window.matchMedia("(orientation: landscape) and (max-width: 991px)").matches) {
    prm.style.cssText = "max-width: 480px; max-height: 480px; margin-bottom: 20px";
}

/* custom css */

// hack pro vycenterování - a pro aplikaci šířky
if ((window.matchMedia("(min-width: 481px)").matches) && (site == "zena")) {
    wrapper.style.cssText = "width: inherit";
}

// respekt.cz a blog.respekt.cz potřeba řešit záporným marginem pro přepis paddingů
if ((window.matchMedia("(orientation: portrait) and (max-width: 414px").matches) && ((site == "ecomag" && area == "respekt" && template == "article") || (site == "respekt" && template == "article"))) {
    wrapper.parentNode.style.cssText = "margin-left: -15px; margin-right: -15px";
}

// částečné řešení chyby ihnedu a starého ekonomu

if (((template == "article") && ((site == "ihned") || (site == "ecomag" && area == "ekonom"))) && (window.matchMedia("(orientation: portrait) and (max-width: 414px)").matches)) {
    prm.style.cssText = "width: " + sDisplay + "px; height: " + sDisplay + "px; left: 50%; margin-left-" + ((sDisplay / 2) + 10) + "px";
}

if (site == "vareni" && area == "recept") {

    /* zde je nutná manipulace s DOM */

    var cssVareni = '.page {padding: 0 0 !important;} .page > header, .page > footer, #recipe-section > .page-detail, #recipe-section > article, .recipe-detail-ingredients, .recipe-detail-desc, recipe-detail-aside {padding: 0 20px !important;}',

        vareniStyle = document.createElement('style');

    wrapper.appendChild(vareniStyle);

    if (vareniStyle.styleSheet) {
        // staré Android/iOS a windows phone
        vareniStyle.styleSheet.cssText = cssVareni;
    } else {
        vareniStyle.appendChild(document.createTextNode(cssVareni));
    }

}