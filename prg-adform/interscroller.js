var site = '%%SITE%%',
    area = '%%AREA%%',
    template = '%%TEMPLATE%%',
    interScope = {},
    pos = '%%POS%%';



// šahám si na medium rectangle na pos=1, abych si dokázal šáhnout na parenta bez ohledu na jeho classu/ID - ta se může kdykoliv změnit
// předpřipraveno na scénář, kdy bychom chtěli pouštět interscroller na pos=2    
try {
    var elementNode = (function() {
        var _sashec = _sashec || parent.window._sashec;
        var pozice = _sashec.getGroupById('default').positions;
        var myElement = (Object.keys(pozice) || []).filter(function(id) {
            var myPos = pozice[id].options.targets.pos;
            if (pozice[id].options.size) {
                return ([].indexOf || String.prototype.indexOf).call(
                    pozice[id].options.size,
                    'mediumrectangle'
                ) != -1 && (myPos == 1 || myPos == 2 || myPos == undefined);
            }
        });

        if (myElement[pos - 1]) {
            return pozice[myElement[pos - 1]].options.element;
        } else {
            return false;
        }
    })();

} catch (e) {}

if (elementNode) {
    ukazme(elementNode);
}



// samotný interscroller kód 
// vkládám lišty, přepisuju kostru a vkládám potřebné media queries
function ukazme(co) {

    var prvniRodicSite = co.parentNode,
        prvniPotomekSite = co.firstChild,
        sDisplaye = screen.width,
        bbxDesign = body.getAttribute('data-bbx-design');

    // horní reklamní lišta
    prvniRodicSite.insertAdjacentHTML('afterbegin', '<div class="listaTop" style="background: rgb(50, 50, 50); color: rgb(255, 255, 255); position: absolute; left: 0;right: 0; width: auto; margin: 0; padding: 5px; text-align: center; text-transform: uppercase; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: 10px; font-family: Helvetica, Arial, sans-serif; display: inline-block; z-index: 100; margin-top: -20px;">Reklama</div>');

    // css pro parenta 2 (nad parent 1)
    prvniRodicSite.style.cssText = "height: 520px; position: relative; margin: 0 auto; overflow: initial; margin-left: -" + (sDisplaye / 2) + "px; margin-right: -" + (sDisplaye / 2) + "px; margin-bottom: 20px";

    // css pro parenta 1 (nad iframe)
    co.style.cssText = "left: 0px; top: 0px; width: 100%; height: 100%; position: absolute; clip: rect(auto, auto, auto, auto); overflow: auto;";

    //css pro iframe
    prvniPotomekSite.style.cssText = "width: 100%; height: 100%; overflow: hidden; position: fixed; top: 0; left: 0;";

    // dolní reklamní lišta
    prvniRodicSite.insertAdjacentHTML('beforeend', '<div class="listaBottom" style="background: rgb(50, 50, 50); color: rgb(255, 255, 255); position: absolute; left: 0; right: 0; width: auto; margin: 0; padding: 5px; text-align: center; text-transform: uppercase; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: 10px; font-family: Helvetica, Arial, sans-serif; display: inline-block; z-index: 100; margin-top: 500px;">Pokračovat dále k obsahu</div>');


    if (window.matchMedia("(orientation: portrait) and (max-width: 414px)").matches) {
        co.style.cssText = "left: 50%; margin-left: -" + (sDisplaye / 2) + "px";
    }

    if (window.matchMedia("(max-width: 849px)").matches) {
        co.style.cssText = "max-width: 480px;";
    }



    // custom js per site - done: aktualne, hps, vareni, zena, ihned, vybermiauto, respekt
    // designové úpravy, potřebné fixes či funkcionality
    if (((site == "aktualne" || site == "zena") && (bbxDesign === "v1" || bbxDesign === "v2")) || (site == "ecomag" && area == "respekt") || (site == "respekt") || (site == "ecomag" && area == "procne")) {

        if (site == "ecomag" && area == "procne") {
            var header = document.querySelector("#brand-c > div.all-wrapper > nav");
        } else {
            var header = document.getElementById('header');
        }

        header.style.cssText = "transition: 0.25s opacity linear;";

        function getTopPos() {
            if (!prvniRodicSite) {
                cleanUp();
            } else {
                var top = co.getBoundingClientRect().top;
                if (top < 60) {
                    header.style.opacity = 0
                }
                if (top > 60) {
                    header.style.opacity = 1
                }
            }
        }

        function getBottomPos() {
            if (!prvniRodicSite) {
                cleanUp();
            } else {
                var bottom = co.getBoundingClientRect().top;
                if (bottom < -35) {
                    header.style.opacity = 1
                }
            }
        }

        var setListeners = setInterval(function() {
            if (!prvniRodicSite) {
                return;
            }
            interScope.hideTop = debounce(getTopPos, 90),
                interScope.hideBottom = debounce(getBottomPos, 90);
            window.addEventListener('scroll', interScope.hideTop);
            window.addEventListener('scroll', interScope.hideBottom);
            clearInterval(setListeners);
        }, 500);
    }

    if ((site == "zena" && area == "hp") || (site == "ecomag" && area == "respekt" && template == "homepage")) {
        prvniRodicSite.parentNode.style.width = "100%";
    }

    if ((site == "ihned" && area == "hp")) {

        prvniRodicSite.parentNode.style.cssText = "width: 100%; margin-left: 0px; margin-right: 0px;";
        prvniRodicSite.style.marginLeft = "-10px";
        prvniRodicSite.style.marginRight = "-10px";

    }

    if ((site == "vybermiauto") || (site == "ecomag" && area == "respekt" && template == "homepage")) {
        prvniRodicSite.parentNode.style.marginLeft = "-15px";
        prvniRodicSite.parentNode.style.marginRight = "0px";
    }

    if (site == "respekt") {
        if (template = "article") {
            prvniRodicSite.style.marginLeft = "-15px";
            prvniRodicSite.style.marginRight = "0px";
        } else {
            prvniRodicSite.style.marginLeft = "-57px";
            prvniRodicSite.style.marginRight = "0px";
        }

    }

    if ((site == "ecomag" && area == "logistika" && template == "list") || (site == "zena" && area == "hp") || (site == "ecomag" && area == "ekonom" && template == "list")) {
        prvniRodicSite.style.marginLeft = "0px";
        prvniRodicSite.style.marginRight = "0px";
    }

    if ((site == "ecomag" && area == "logistika" && template == "article") || (site == "ecomag" && area == "ekonom" && template == "article")) {
        prvniRodicSite.style.marginLeft = "0px";
        prvniRodicSite.style.marginRight = "-30px";
    }

    if (site == "ecomag" && area == "procne") {
        if (template == "article") {
            prvniRodicSite.style.marginLeft = "-20px";
            prvniRodicSite.style.marginRight = "0px";
        } else {
            prvniRodicSite.style.marginLeft = "-45px";
            prvniRodicSite.style.marginRight = "0px";
        }
    }


    // helpers pro některé funkcionality
    // debounce, cleaning funkce
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate)
                    func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow)
                func.apply(context, args);
        };
    };

    function cleanUp() {
        header.removeAttribute("style");

        if (typeof(interScope.hideTop) === 'function' && typeof(interScope.hideBottom) === 'function') {
            window.removeEventListener('scroll', interScope.hideTop);
            window.removeEventListener('scroll', interScope.hideBottom);
        }
    }


}